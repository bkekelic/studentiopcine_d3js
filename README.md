﻿Projektni zadatak iz kolegija Vizualizacija podataka.

Cilj projektnog zadatka je vizualizirati prikaz karte sa svim općinama u Republici Hrvatskoj, 
te zatim omogućiti prikazivanje dodatnih informacija o odabranoj općini na pojedinom grafu. 
Također bilo je potrebno omogućiti usporedni prikaz detaljnih informacija o dvije odabrane općine na jednom grafu. 
Osim toga, korisnicima je potrebno omogućiti pretraživanje samih općina kako bi, ukoliko ne znaju gdje se točno nalazi 
tražena općina, ipak mogli pronaći i uvidjeti informacije o njoj samoj.  

Web stranicu je potrebno pokrenuti preko Google-ove aplikacije: https://chrome.google.com/webstore/detail/web-server-for-chrome/ofhbbkphhbklhfoeikjpcbhemlocgigb

Upotreba:
Prelaskom pokazivača preko pojedine općine, ispisuje se naziv te općine odmah iznad trenutne lokacije pokazivača.
Klikom na odabranu općinu na grafu se prikazuju podatci o upisanim studentima za akademsku godinu 2017./18. u pojedino učilište.
Prelaskom pokazivača preko bilo koje druge općine, ona se prikazuje na grafu plavom bojom, zajedno s odabranom općinom.
Prelaskom pokazivača preko stupčastog grafa ispisuje se vrijednost stupca odmah iznad trenutne lokacije pokazivača.

Omogućeno je pretraživanje općina po njihovim nazivima, kako za crvenu općinu, tako i za plavu općinu.

Bernard Kekelić,
Osijek, 2019.